class ProductFilter
  attr_reader :taxon, :view, :sort, :option_value, :keyword
  VALID_PARAMETER = %i(view sort tshirt-size tshirt-color keyword).freeze

  def initialize(params = {})
    @taxon = params[:taxon].presence
    @view = params[:view] || :grid
    @sort = params[:sort].presence
    @option_value = params['tshirt-color'].presence || params['tshirt-size'].presence
    @keyword = params[:keyword].presence
  end

  def filtered_products
    scopes = get_base_scopes(taxon)
    scopes = scopes.filter_with_option_value(option_value) if option_value
    scopes = scopes.search_with(keyword) if keyword
    scopes =
      case sort
      when 'price_desc' then scopes.price_desc
      when 'price_asc'  then scopes.price_asc
      when 'oldest'     then scopes.oldest
      else scopes.newest
      end
    scopes
  end

  def sort_select_collection
    { '新着順': 'newest',
      '値段の高い順': 'price_desc',
      '値段の低い順': 'price_asc',
      '古い順': 'oldest' }
  end

  def list_view?
    view == 'list'
  end

  private

  def get_base_scopes(taxon = nil)
    return Spree::Product.in_taxon(taxon) if taxon

    Spree::Product.all
  end
end
