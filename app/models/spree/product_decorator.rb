Spree::Product.class_eval do
  # @params root [Spree::Taxonomy]
  # @return [Array]
  def related_products(root = Spree::Taxon.root)
    return [] if taxons.empty?
    taxons.where(taxonomy_id: root.id).includes(:products).map do |taxon|
      taxon.products.reject { |product| product.id == id }
    end.flatten.uniq
  end

  scope :newest, -> { available.reorder(available_on: :desc).distinct }
  scope :oldest, -> { available.reorder(available_on: :asc).distinct }
  scope :price_asc,  -> { select('spree_products.*, spree_prices.amount').joins(master: :default_price).reorder('spree_prices.amount asc').distinct }
  scope :price_desc, -> { select('spree_products.*, spree_prices.amount').joins(master: :default_price).reorder('spree_prices.amount desc').distinct }
  scope :search_with, -> (keyword) { where('name LIKE :keyword OR description LIKE :keyword',
                                                    keyword: "%#{sanitize_sql_like(keyword)}%") }

  scope :filter_with_option_value, -> (names) {
    joins(variants: :option_values)
      .where(spree_option_values: { name: names })
  }
end
