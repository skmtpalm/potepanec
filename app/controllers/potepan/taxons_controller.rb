module Potepan
  class TaxonsController < Potepan::StoreController
    def show
      @taxon = Spree::Taxon.find(params[:taxon_id])
      @roots = Spree::Taxon.roots

      @product_filter = ProductFilter.new(filter_params.merge(taxon: @taxon))
      @products = @product_filter.filtered_products
    end

    private

    def filter_params
      params.permit(*ProductFilter::VALID_PARAMETER)
    end
  end
end
