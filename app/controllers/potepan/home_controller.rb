module Potepan
  class HomeController < Potepan::StoreController
    FEATURE_CATEGORIES_NAME = %w(T-Shirts Bags Mugs).freeze
    def index
      @feature_categories = Spree::Taxon.where(name: FEATURE_CATEGORIES_NAME)
      @latest_products = Spree::Product.newest
    end
  end
end
