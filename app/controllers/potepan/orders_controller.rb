module Potepan
  class OrdersController < Potepan::StoreController
    def edit
      @order = current_order || Spree::Order.incomplete.find_or_initialize_by(guest_token: cookies.signed[:guest_token])
    end

    def update
    end

    def add_cart
      @order = current_order(create_order_if_necessary: true)
      variant = Spree::Variant.find(params[:variant_id])
      quantity = params[:quantity].to_i

      if !quantity.between?(1, 2_147_483_647)
        @order.errors.add(:base, '適正な量を入力してください')
      end

      begin
        @line_item = @order.contents.add(variant, quantity)
      rescue ActiveRecord::RecordInvalid => e
        @order.errors.add(:base, e.record.errors.full_messages.join(', '))
      end

      if @order.errors.any?
        flash[:error] = @order.errors.full_messages.join(', ')
        redirect_back_or_default potepan_root_path
      else
        redirect_to potepan_cart_url
      end
    end
  end
end
