module Potepan
  class ProductsController < Potepan::StoreController
    RELATED_ITEM_COUNT = 4

    before_action :set_roots, only: [:index, :search]

    def index
      @product_filter = ProductFilter.new(filter_params)
      @products = @product_filter.filtered_products
    end

    def show
      @product = Spree::Product.friendly.find(params[:slug_or_id])
      @taxonomy = Spree::Taxonomy.find_by(name: 'Categories') || Spree::Taxon.root
      @related_products = @product.related_products.sample(RELATED_ITEM_COUNT)
    end

    def search
      @product_filter = ProductFilter.new(filter_params)
      @products = @product_filter.filtered_products

      redirect_to potepan_products_url unless params[:keyword].presence
    end

    private

    def set_roots
      @roots = Spree::Taxon.roots
    end

    def filter_params
      params.permit(*ProductFilter::VALID_PARAMETER)
    end
  end
end
