require 'rails_helper'

RSpec.describe Potepan::OrdersController, type: :controller do
  let!(:store) { create(:store) }
  let(:variant) { create(:variant) }
  let(:quantity) { 1 }

  describe 'POST #add_cart' do
    it 'カートページへリダイレクトする' do
      post :add_cart, params: { variant_id: variant.id, quantity: quantity }
      expect(response).to redirect_to potepan_cart_url
    end

    it '新しい order が作られる' do
      expect do
        post :add_cart, params: { variant_id: variant.id, quantity: quantity }
      end.to change(Spree::Order, :count).by(1)
    end

    it '新しい lineitem が作られる' do
      expect do
        post :add_cart, params: { variant_id: variant.id, quantity: quantity }
      end.to change(Spree::LineItem, :count).by(1)
    end

    context '異常系' do
      context '数量が範囲外の場合' do
        it '@order に error が追加される' do
          post :add_cart, params: { variant_id: variant.id, quantity: -1 }
          expect(assigns(:order).errors).not_to be_empty
        end

        it 'error メッセージが正しい' do
          post :add_cart, params: { variant_id: variant.id, quontity: -1 }
          error_message = assigns(:order).errors.messages[:base][0]
          expect(error_message).to eq '適正な量を入力してください'
        end
      end

      context 'リダイレクトの振り分け' do
        it "一つ前の画面にリダイレクト" do
          session[:spree_user_return_to] = potepan_product_path(variant.product.id)
          post :add_cart, params: { variant_id: variant.id, quantity:  -1 }
          expect(response).to redirect_to potepan_product_path(variant.product.id)
        end

        it 'デフォルトでは potepan_root へ戻る' do
          post :add_cart, params: { variant_id: variant.id, quantity:  -1 }
          expect(response).to redirect_to potepan_root_path
        end
      end
    end
  end

  describe 'GET #edit' do
    it 'edit temaplate をレンダリング' do
      get :edit
      expect(response).to render_template :edit
    end

    context 'カートに商品が入ってる場合' do
      it 'レスポンス成功' do
        post :add_cart, params: { variant_id: variant.id, quantity: quantity }
        get :edit
        expect(response).to have_http_status :success
      end
    end

    context 'カートに商品が入っていない場合' do
      it 'レスポンス成功' do
        get :edit
        expect(response).to have_http_status :success
      end
    end
  end
end
