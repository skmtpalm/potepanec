require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe 'GET #index' do
    it 'レスポンスが成功' do
      get :index
      expect(response).to have_http_status(:success)
    end

    describe 'latest_products' do
      let(:products) do
        [
          create(:product, available_on: 1.day.ago),
          create(:product, available_on: 2.days.ago),
          create(:product, available_on: 3.days.ago)
        ]
      end
      let(:not_available_product) { create(:product, available_on: nil) }
      let(:available_on_future_product) do
        create(:product, available_on: 2.days.since)
      end

      it '利用可能な新着商品が利用可能日順に並ぶ' do
        get :index
        expect(assigns(:latest_products)).to match(products)
      end

      it '利用可能ではない商品は含まれないこと' do
        products << not_available_product
        get :index
        expect(assigns(:latest_products)).to_not include(not_available_product)
      end

      it 'まだ利用可能ではない商品は含まれないこと' do
        products << available_on_future_product
        get :index
        expect(assigns(:latest_products))
          .to_not include(available_on_future_product)
      end
    end
  end
end
