require 'rails_helper'

RSpec.describe Potepan::TaxonsController, type: :controller do
  describe 'GET #show:taxon_id' do
    let(:root_taxon) { create(:taxon) }
    context 'ルートカテゴリーの場合' do
      it 'レスポンスが成功' do
        get :show, params: { taxon_id: root_taxon.id }
        expect(response).to have_http_status(:success)
      end

      it 'parent_id を持っていない' do
        get :show, params: { taxon_id: root_taxon.id }
        expect(assigns(:taxon).parent_id).to be nil
        expect(assigns(:taxon).root?).to be true
      end
    end

    context '子カテゴリーの場合' do
      let(:child_taxon) { create(:taxon, parent_id: root_taxon.id) }
      it 'レスポンスが成功' do
        get :show, params: { taxon_id: child_taxon.id }
        expect(response).to have_http_status(:success)
      end

      it 'parent_id を持っている' do
        get :show, params: { taxon_id: child_taxon.id }
        expect(assigns(:taxon).parent_id).to eq(root_taxon.id)
        expect(assigns(:taxon).root?).to be false
      end
    end

    context 'params[:view] が list の場合' do
      it 'レスポンスが成功' do
        get :show, params: { taxon_id: root_taxon.id, view: 'list' }
        expect(response).to have_http_status(:success)
      end
    end

    context 'params[:view] が list以外の場合' do
      it 'レスポンスが成功' do
        get :show, params: { taxon_id: root_taxon.id, view: nil }
        expect(response).to have_http_status(:success)
      end
    end

    context 'パラメーターに表示順番指定(sort)のパラメーターが含まれていたら' do
      let(:sorted_products_with_taxon) do
        [
          create(:product, price: 100, available_on: 3.days.ago) do |product|
            product.taxons << root_taxon
          end,
          create(:product, price: 120, available_on: 2.days.ago) do |product|
            product.taxons << root_taxon
          end,
          create(:product, price: 130, available_on: 1.day.ago) do |product|
            product.taxons << root_taxon
          end
        ]
      end

      context 'sort=price_descの場合' do
        it 'レスポンス成功' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'price_desc' }
          expect(response).to have_http_status(:success)
        end

        it '@products は値段が高い順に並ぶ' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'price_desc' }
          expect(assigns(:products)).to match(
            [
              sorted_products_with_taxon[2],
              sorted_products_with_taxon[1],
              sorted_products_with_taxon[0]
            ]
          )
        end
      end

      context 'sort=price_ascの場合' do
        it 'レスポンス成功' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'price_asc' }
          expect(response).to have_http_status(:success)
        end

        it '@products は値段が低い順に並ぶ' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'price_asc' }
          expect(assigns(:products)).to match(
            [
              sorted_products_with_taxon[0],
              sorted_products_with_taxon[1],
              sorted_products_with_taxon[2]
            ]
          )
        end
      end

      context 'sort=newestの場合' do
        it 'レスポンス成功' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'newest' }
          expect(response).to have_http_status(:success)
        end

        it '@products は新着順に並ぶ' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'newest' }
          expect(assigns(:products)).to match(
            [
              sorted_products_with_taxon[2],
              sorted_products_with_taxon[1],
              sorted_products_with_taxon[0]
            ]
          )
        end
      end

      context 'sort=newestの場合' do
        it 'レスポンス成功' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'oldest' }
          expect(response).to have_http_status(:success)
        end

        it '@products は古い順に並ぶ' do
          get :show, params: { taxon_id: root_taxon.id, sort: 'oldest' }
          expect(assigns(:products)).to match(
            [
              sorted_products_with_taxon[0],
              sorted_products_with_taxon[1],
              sorted_products_with_taxon[2]
            ]
          )
        end
      end

      context 'デフォルトは新着順(not params[:sort])' do
        it '@products は新着順に並ぶ' do
          get :show, params: { taxon_id: root_taxon.id }
          expect(assigns(:products)).to match(
            [
              sorted_products_with_taxon[2],
              sorted_products_with_taxon[1],
              sorted_products_with_taxon[0]
            ]
          )
        end
      end
    end
  end
end
