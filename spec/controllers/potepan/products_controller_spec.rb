require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product) { create(:product) }

  describe 'GET #index' do
    let(:option_type_color) do
      create(:option_type, name: 'color', presentation: 'Color')
    end

    let(:option_value_color) do
      create(:option_value, name: 'Red',
                            presentation: 'Red',
                            option_type: option_type_color)
    end

    let(:option_type_size) do
      create(:option_type, name: 'size', presentation: 'Size')
    end

    let(:option_value_size) do
      create(:option_value, name: 'Medium',
                            presentation: 'M',
                            option_type: option_type_size)
    end

    let(:products_with_color) do
      %w(tshirt shirt pants).map do |name|
        create(:product, name: "red-#{name}") do |product|
          product.option_types << option_type_color
        end
      end
    end

    let(:products_with_size) do
      %w(tshirt shirt pants).map do |name|
        create(:product, name: "M-#{name}") do |product|
          product.option_types << option_type_size
        end
      end
    end

    let(:products_with_size_and_color) do
      %w(tshirt shirt pants).map do |name|
        create(:product, name: "Red-M-#{name}") do |product|
          product.option_types << option_type_color
          product.option_types << option_type_size
        end
      end
    end

    it 'レスポンス成功' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'index template をレンダリング' do
      get :index
      expect(response).to render_template :index
    end

    it '全ての製品が @product に割り当てられる' do
      get :index
      expect(assigns(:products)).to match_array(
        products_with_color + products_with_size + products_with_size_and_color
      )
    end

    context 'パラメーターにフィルタークエリが含まれていたら' do
      it 'レスポンス成功' do
        get :index,
            params: { option_type_color.name.to_sym => option_value_color.name }
        expect(response).to have_http_status(:success)
      end

      it 'index template をレンダリング' do
        get :index,
            params: { option_type_color.name.to_sym => option_value_color.name }
        expect(response).to render_template :index
      end

      it '要求された製品が @product に割り当てられる' do
        get :index,
            params: { option_type_color.name.to_sym => option_value_color.name }
        expect(assigns(:products)).to match_array(
          products_with_color + products_with_size_and_color)
      end

      it '他の製品が @product に含まれない' do
        get :index,
            params: { option_type_size.name.to_sym => option_value_size.name }
        expect(assigns(:products)).to_not include(products_with_color)
      end
    end

    context 'パラメーターに表示順番指定(sort)のパラメーターが含まれていたら' do
      let(:sorted_products) do
        [
          create(:product, price: 100, available_on: 3.days.ago),
          create(:product, price: 120, available_on: 2.days.ago),
          create(:product, price: 130, available_on: 1.day.ago)
        ]
      end

      context 'sort=price_desc の場合' do
        it 'レスポンス成功' do
          get :index, params: { sort: 'price_desc' }
          expect(response).to have_http_status(:success)
        end

        it '値段が高い順に並ぶ' do
          get :index, params: { sort: 'price_desc' }
          expect(assigns(:products)).to match(
            [
              sorted_products[2],
              sorted_products[1],
              sorted_products[0]
            ]
          )
        end
      end

      context 'sort=price_asc の場合' do
        it 'レスポンス成功' do
          get :index, params: { sort: 'price_asc' }
          expect(response).to have_http_status(:success)
        end

        it '値段が低い順に並ぶ' do
          get :index, params: { sort: 'price_asc' }
          expect(assigns(:products)).to match(
            [
              sorted_products[0],
              sorted_products[1],
              sorted_products[2]
            ]
          )
        end
      end

      context 'sort=newest の場合' do
        it 'レスポンス成功' do
          get :index, params: { sort: 'newest' }
          expect(response).to have_http_status(:success)
        end

        it '新着順に並ぶ' do
          get :index, params: { sort: 'newest' }
          expect(assigns(:products)).to match(
            [
              sorted_products[2],
              sorted_products[1],
              sorted_products[0]
            ]
          )
        end
      end

      context 'sort=oldest の場合' do
        it 'レスポンス成功' do
          get :index, params: { sort: 'oldest' }
          expect(response).to have_http_status(:success)
        end

        it '古い順に並ぶ' do
          get :index, params: { sort: 'oldest' }
          expect(assigns(:products)).to match(
            [
              sorted_products[0],
              sorted_products[1],
              sorted_products[2]
            ]
          )
        end
      end

      context 'デフォルトは新着順(not params[:sort])' do
        it '新着順に並ぶ' do
          get :index
          expect(assigns(:products)).to match(
            [
              sorted_products[2],
              sorted_products[1],
              sorted_products[0]
            ]
          )
        end
      end
    end
  end

  describe 'GET #search' do
    context 'パラメーターに keyword が含まれていた場合' do
      #
      # let           | fruit | vegetable |  juice  |   jam   |  100%?  |
      # -----------------------------------------------------------------
      # orange        | true  | -         |  true   | -       |  true   |
      # apple         | true  | -         |  -      | true    |  true   |
      # banana        | true  | -         |  true   | -       |  -      |
      # melon         | -     | true      |  true   | -       |  -      |
      #
      let!(:orange) { create(:product,name: 'orange-juice', description: 'Made with 100% fruit juice') }
      let!(:apple) { create(:product, name: 'apple-jam', description: 'Made with 100% fruit jam') }
      let!(:banana) { create(:product, name: 'banana-juice', description: 'very popular fruit juice') }
      let!(:melon) { create(:product, name: 'melon-juice', description: 'very sweet vegetable juice') }

      it 'search template をレンダリング' do
        get :search, params: { keyword: 'fruit' }
        expect(response).to render_template :search
      end

      context 'keyword=fruit' do
        it 'レスポンス成功' do
          get :search, params: { keyword: 'fruit' }
          expect(response).to have_http_status :success
        end

        it '要求された商品(fruitを含む)が @products に割り当てられる' do
          get :search, params: { keyword: 'fruit' }
          expect(assigns(:products)).to match_array([orange, apple, banana])
        end

        it '野菜(melon)は含まれない' do
          get :search, params: { keyword: 'fruit' }
          expect(assigns(:products)).to_not include(melon)
        end
      end

      context 'keyword=juice' do
        it 'レスポンス成功' do
          get :search, params: { keyword: 'juice' }
          expect(response).to have_http_status :success
        end

        it '要求された商品(juice)が @products に割り当てられる' do
          get :search, params: { keyword: 'juice' }
          expect(assigns(:products)).to match_array([orange, banana, melon])
        end

        it 'Jam(apple) は含まれない' do
          get :search, params: { keyword: 'juice' }
          expect(assigns(:products)).to_not include(apple)
        end
      end

      context 'keyword=100%(include meta chara)' do
        it 'レスポンス成功' do
          get :search, params: { keyword: '100%' }
          expect(response).to have_http_status :success
        end

        it '要求された製品(100%)が @products に割り当てられる' do
          get :search, params: { keyword: '100%' }
          expect(assigns(:products)).to match_array([orange, apple])
        end
      end

      context 'keyword= が空の場合' do
        it '商品一覧(products#index)へリダイレクト' do
          get :search, params: { keyword: '' }
          expect(response).to redirect_to potepan_products_path
        end

        it '全ての商品が割り当てられる' do
          get :search, params: { keyword: '' }
          expect(assigns(:products)).to match_array([orange, apple, banana, melon])
        end
      end

      context '商品に含まれない keyword が指定されたら' do
        it 'レスポンス成功' do
          get :search, params: { keyword: 'jelly' }
          expect(response).to have_http_status :success
        end

        it '@products は空' do
          get :search, params: { keyword: 'jelly' }
          expect(assigns(:products)).to match([])
        end
      end
    end

    context 'パラメーターに keyword が含まれていない場合' do
      it '商品一覧(products#index)へリダイレクト' do
        get :search
        expect(response).to redirect_to potepan_products_path
      end
    end
  end

  describe 'GET #show:slug_or_id' do
    context 'params が id の場合' do
      before(:each) do
        get :show, params: { slug_or_id: product.id }
      end

      it 'レンスポンスが成功' do
        expect(response).to have_http_status :success
      end

      it '要求された製品が @product に割り当てられる' do
        expect(assigns(:product)).to eq product
      end

      it 'show templateをレンダリング' do
        expect(response).to render_template :show
      end
    end

    context 'params が slug の場合' do
      before(:each) do
        get :show, params: { slug_or_id: product.slug }
      end

      it 'レンスポンスが成功' do
        expect(response).to have_http_status :success
      end

      it '要求された製品が @product に割り当てられる' do
        expect(assigns(:product)).to eq product
      end

      it 'show templateをレンダリング' do
        expect(response).to render_template :show
      end
    end

    describe 'related_products' do
      let(:taxonomy) { create(:taxonomy, name: 'Categories') }
      let(:table) do
        taxonomy.root.children.create(name: 'Table', taxonomy: taxonomy)
      end
      let(:chair) do
        taxonomy.root.children.create(name: 'Chair', taxonomy: taxonomy)
      end
      let(:table_products) do
        %w(long middle short).map do |name|
          create(:product, name: "#{name}-table") do |product|
            product.taxons << table
          end
        end
      end

      let(:chair_products) do
        %w(hi school low).map do |name|
          create(:product, name: "#{name}-chair") do |product|
            product.taxons << chair
          end
        end
      end

      it '関連する製品に同じカテゴリーの製品が割り当てられている' do
        target_table = table_products.first
        get :show, params: { slug_or_id: target_table.slug }
        expect(assigns(:related_products)).to match_array(table_products -
                                                                [target_table])
      end

      it '他のカテゴリーは含まれない' do
        target_chair = chair_products.first
        get :show, params: { slug_or_id: target_chair.slug }
        expect(assigns(:related_products)).to_not include(table_products)
      end

      it '個数は４つ' do
        target_table = table_products.first
        get :show, params: { slug_or_id: target_table.slug }
        expect(assigns(:related_products).count <= 4).to be_truthy
      end
    end
  end
end
