require 'rails_helper'

RSpec.feature 'Cart', type: :feature do
  background do
    create(:taxon)
    create(:variant)
    create(:store)
  end

  scenario '商品詳細ページから商品をカートに入れる' do
    product = Spree::Product.first
    visit potepan_product_path(product)

    # ページタイトルに product name が含まれる
    expect(page).to have_title(product.name)
    select '2', from: 'quantity'
    click_on 'カートへ入れる'

    # カートに入れると、cart ページにリダイレクトされる
    expect(current_url).to eq potepan_cart_url

    # lineitem の quontity の値が正しい
    expect(find('#order_line_items_attributes_0_quantity').native['value']).to eq '2'

    # もう一度product の詳細ページに戻って、カードに追加
    visit potepan_product_path(product)

    select '1', from: 'quantity'
    click_on 'カートへ入れる'

    # lineitem の quontity の値が増えている 2 -> 3
    expect(find('#order_line_items_attributes_0_quantity').native['value']).to eq '3'
  end

  scenario 'グローバルナビのカート表示' do
    product = Spree::Product.first
    cart_dropdown = '.topBar .pull-right > li:nth-of-type(3) > ul'

    # 最初に商品は入っていない
    visit potepan_root_path
    expect(find(cart_dropdown + ' li:first-child').native.text)
      .to eq 'カートに商品はありません'

    # 商品を追加するために product ページへ
    visit potepan_product_path(product)
    select '1', from: 'quantity'
    click_on 'カートへ入れる'

    # order の lineitemの総数が表示されている
    expect(find(cart_dropdown + ' li:first-child')
      .native.text).to eq "カートに商品が #{Spree::Order.last.line_items.count} 点あります"

    # 追加した productが表示されている(カートに入れた商品へのリンクが正しく表示されている)
    expect(find(cart_dropdown + ' li:nth-of-type(2) > a')
      .native.attributes['href'].value).to eq potepan_product_path(product)
  end
end
